/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import inscricao.entity.Idioma;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author wili
 */
@Named
@RequestScoped
public class RegistroBean extends PageBean{
    
    private ArrayList<Candidato> CandidatoList;
    
    private static final Idioma[] IDIOMAS = {
        new Idioma(1, "Inglês"),
        new Idioma(2, "Alemão"),
        new Idioma(3, "Francês")
    };
    
    private Candidato candidato = new Candidato(IDIOMAS[0]); // inicialmente ingles
    private List<SelectItem> idiomaItemList;
    private ArrayDataModel<Idioma> idiomasDataModel;
    
    public RegistroBean(){
        this.CandidatoList = new ArrayList<>();
    }
    
    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public Idioma[] getIdiomas() {
        return IDIOMAS;
    }
    
    public List<SelectItem> getIdiomaItemList() {
        if (idiomaItemList != null) return idiomaItemList;
        idiomaItemList = new ArrayList<>();
        for (Idioma id: IDIOMAS) {
            idiomaItemList.add(new SelectItem(id.getCodigo(), id.getDescricao()));
        }
        return idiomaItemList;
    }
    
    public ArrayDataModel<Idioma> getIdiomasDataModel() {
        if (idiomasDataModel == null) {
            idiomasDataModel = new ArrayDataModel<>(IDIOMAS);
        }
        return idiomasDataModel;
    }

    public String confirmaAction() {
        candidato.setDataHora(new Date());
        candidato.setIdioma(IDIOMAS[candidato.getIdioma().getCodigo()-1]);
        return "confirma";
    }

    public ArrayList<Candidato> getCandidatoList() {
        return CandidatoList;
    }

    public void setCandidatoList(ArrayList<Candidato> CandidatoList) {
        this.CandidatoList = CandidatoList;
    }

    public void addCandToArray(Candidato c){
        this.CandidatoList.add(c);
    }
    
}
