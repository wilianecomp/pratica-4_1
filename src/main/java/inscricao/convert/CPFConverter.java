/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.convert;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author wili
 */
@FacesConverter("cpfConverter")
public class CPFConverter implements Converter{
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value)
        throws ConverterException {
        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value)
        throws ConverterException {
        return "DeuRuim";
    }
}
